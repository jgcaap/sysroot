#!/usr/bin/env python3
#
# This file is part of Checkbox.
#
# Copyright (C) 2010-2013 by Cloud Computing Center for Mobile Applications
# Industrial Technology Research Institute
#
# Authors:
#   Nelson Chu <Nelson.Chu@itri.org.tw>
#   Jeff Lane <jeff@ubuntu.com>
#
# Checkbox is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3,
# as published by the Free Software Foundation.
#
# Checkbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Checkbox.  If not, see <http://www.gnu.org/licenses/>.
"""
disk_info

Uses lshw to gather information about disks seen by the OS.
Outputs logical name, vendor, description, size and product data
"""

import sys
import xml.etree.ElementTree as ET
from subprocess import check_output


def get_item(disk, attribute):
    try:
        return disk.find(attribute).text
    except AttributeError:
        return "Unknown"


def main():
    hwinfo_xml = check_output(['lshw', '-c', 'disk', '-xml'])
    root = ET.fromstring(hwinfo_xml)

    # Parse lshw XML for gathering disk information.
    disk_list = root.findall(".//node[@class='disk']")

    if not disk_list:
        print("No disk information discovered.")
        return 10

    for disk in disk_list:
        if disk.get('id') == 'disk':
            print("Name: {}".format(get_item(disk, 'logicalname')))
            print("\t{k:15}\t{v}".format(k="Description:",
                                         v=get_item(disk, 'description')))
            print("\t{k:15}\t{v}".format(k="Vendor:",
                                         v=get_item(disk, 'vendor')))
            print("\t{k:15}\t{v}".format(k="Product:",
                                         v=get_item(disk, 'product')))
            try:
                disk_size = ("%dGB" % (
                    int(disk.find('size').text) / (1000**3)))
            except TypeError:
                disk_size = "No Reported Size"
            except AttributeError:
                disk_size = "No Reported Size"
            print("\t{k:15}\t{v}".format(k="Size:",
                                         v=disk_size))
    return 0

if __name__ == '__main__':
    sys.exit(main())
