#!/usr/bin/env python3

# compressor.py
from subprocess import Popen, PIPE

def compress(value):
    """Compresses a byte array with the xz binary"""

    process = Popen(["xz", "--compress", "--force"], stdin=PIPE, stdout=PIPE)
    return process.communicate(value)[0]

def decompress(value):
    """Decompresses a byte array with the xz binary"""

    process = Popen(["xz", "--decompress", "--stdout", "--force"],
                    stdin=PIPE, stdout=PIPE)
    return process.communicate(value)[0]

def compress_file(path):
    """Compress the file at 'path' with the xz binary"""

    process = Popen(["xz", "--compress", "--force", "--stdout", path], stdout=PIPE)
    return process.communicate()[0]

# compressor.py

import os
import sys
from optparse import OptionParser
from sys import argv
import base64
try:
    import cPickle as pickle
except ImportError:
    import pickle
from io import BytesIO

from os.path import basename
from errno import EPIPE

def load():
    ppds_compressed = base64.b64decode(ppds_compressed_b64)
    ppds_decompressed = decompress(ppds_compressed)
    ppds = pickle.loads(ppds_decompressed)
    return ppds

def ls():
    binary_name = basename(argv[0])
    ppds = load()
    for key, value in ppds.items():
        if key == 'ARCHIVE': continue
        for ppd in value[2]:
            try:
                print(ppd.replace('"', '"' + binary_name + ':', 1))
            except IOError as e:
                # Errors like broken pipes (program which takes the standard
                # output terminates before this program terminates) should not
                # generate a traceback.
                if e.errno == EPIPE: exit(0)
                raise

def cat(ppd):
    # Ignore driver's name, take only PPD's
    ppd = ppd.split(":")[-1]
    # Remove also the index
    ppd = "0/" + ppd[ppd.find("/")+1:]

    ppds = load()
    ppds['ARCHIVE'] = BytesIO(decompress(ppds['ARCHIVE']))

    if ppd in ppds:
        start = ppds[ppd][0]
        length = ppds[ppd][1]
        ppds['ARCHIVE'].seek(start)
        return ppds['ARCHIVE'].read(length)

def main():
    usage = "usage: %prog list\n" \
            "       %prog cat URI"
    version = "%prog 1.0.2\n" \
              "Copyright (c) 2013 Vitor Baptista.\n" \
              "This is free software; see the source for copying conditions.\n" \
              "There is NO warranty; not even for MERCHANTABILITY or\n" \
              "FITNESS FOR A PARTICULAR PURPOSE."
    parser = OptionParser(usage=usage,
                          version=version)
    (options, args) = parser.parse_args()

    if len(args) == 0 or len(args) > 2:
        parser.error("incorrect number of arguments")

    if args[0].lower() == 'list':
        ls()
    elif args[0].lower() == 'cat':
        if not len(args) == 2:
            parser.error("incorrect number of arguments")
        ppd = cat(args[1])
        if not ppd:
            parser.error("Printer '%s' does not have default driver!" % args[1])
        try:
            # avoid any assumption of encoding or system locale; just print the
            # bytes of the PPD as they are
            if sys.version_info.major < 3:
                sys.stdout.write(ppd)
            else:
                sys.stdout.buffer.write(ppd)
        except IOError as e:
            # Errors like broken pipes (program which takes the standard output
            # terminates before this program terminates) should not generate a
            # traceback.
            if e.errno == EPIPE: exit(0)
            raise
    else:
        parser.error("argument " + args[0] + " invalid")

# PPDs Archive
ppds_compressed_b64 = b"/Td6WFoAAATm1rRGAgAhARYAAAB0L+Wj4CpMGYxdAEAAyynXgKBkJ1IWv/boY8vTM9MktbLOpatIGjz3QEXi/jpWYIhyvau9pwPzeQg+zs296STl+/651JV9bUWSsacdcfyylbDsflM5cFqUPPFbQGtopEPeknVDKbujm46eJZT+uFWAbTyLoheOP7J59CVqfJ2en9zO22jpXObw3DtlWFZZ3i0GshaE756Mxutikhtx8yaAuu7nE33eWIpx/z9iAImDmFKYswhidDJAxy1V03nZbWb8f1nwZC3qeUh/tKmkMWj3pjwedsC2CMV/snkQtHnAe9U3x70sgLa7tpO44i+SYGBXeJ2aVoS//yhCzYqyGFdlmPQBiJrcfZV4GOeiyWnN5cDu3XsBNYJ6uE+A9Qqpk/6DO/kxJ+mJWMB0/hK6dENPZ350WQc3vdK8MRNnN2+T/A72M2qIIzh0XS8PdygIXujj0kPNX0yTVpzxJyyapdiVT5jnyeTGaQLnSXIfAr6eHKvzXc+BLd0nwKgkxRmr6D86ZvDmYlQJRgsAT30qxOT9qCtELQflvR6i7cTqpgMOjR+LfS398smj4oFL5JcIh7KScvIUpCIgDj02l5nVF9KQzGKk71dbQOJzqdZkdCNBXtEU7LKa6CSl1RInA5V3pEZZq80DY22JfSKGkyqujvk69aRDBm9A614t10U7wh3iyP8bt6Iuddhcu443GsIaTw5kS3/d9/yUApm0NfVce0bxEiqx0C579EePlab4W2JA74lxkRJy7qeAyKAKxEzp14OqD9eCmdbPTE8KXK1kFxeS7g5zOySnol06ihITgad8Gq91gXdX723nIu5LRMJNAPNLmfIn9+3E9S5ZxBAY1QXH23RAxBSdEQRyL+zF8PqDHgu3IqM5sKZ7aEjuc2BjwjwFLLRKfRNhz+y5sz0sleah85tBcowlXruF2LK31gA2bITMv7QuIUH1BIeRtJ2RPrbFCajNdn4djLHk5V7mxq9qsU0zDgn28XKlC9YN2giccFXiNNoPkWlqsEpPAW9usIEm0PT+Xc5ZLbVTgKRBglwy2U12HmT+PEunKgyM/V8kV47ubKzGpirpIs8uvt2iHAYPmaqA1xDFeZ4dw2iDUK5H+OveuBT7RAQrjSce1hBHX/w462vf17kAKzwIWT1uNpcCXzJx+QDnTvNGkQ5MQB/3CAVL4SrvVHhQhBvgb5+hfvcjQaapyByn440nueK7HPBV3sBJvBsSRgLslrUDZ6RH63P7jO0prRpxtLZj6lzSEW7T6hcp79+PuOd0zmcgZmkgeXlGxvQKq3QV0KpFM7YCqa7sSSZn9sHgEOqYnCXkr2C4ttqc8JYw+1TrXWstVKigBh9NUD7PwlNxp4rV+jv4m15t04/0SaKYq67bg/+iPVmWJXtUzTQFtJ7KG8zsaM0X2c/8H0RJQHM4IWCyKmMlCIUdgRN8MKTPtuN+bQWn3Oal25nAhYOp1M00FKa4/lpN4LEfEC3ZYqa7wFCtvxSD3wBiKDM37DD51cQ2jlyOPSSHTixloLwpjWGiZYcJ3dKo9jLVKst8O8WSlGxakjF94QYPzDurs5hjgFrRC1kUw/Rut1cUo68n5a2MOhSqlAbXbkdhOGOcdCMPpLAF22owNzrjLhmpDakUU9Q87cQT/dLystkna00cQDq9otpr9V9P0L7PnzcozPXAhuvsT1P5AuGszrE0G56di/MQkE/+U5KdkfIR0na3cfoNH+APHiQM2YsLPRR88Kv+7buOusayQUOOyq5NIJh/Ifa5jPqMq6yCVY4ilW2mLOdp89DgjxJV95bSN770iAqLPvA0c97QMyPCPocUvROiwHk7RJzxBI6yrKC7cu/la5+FXdJaR1sLgjOLXBCaT9FnuWUkL0Sn/a79cZLROG7YQq3FezARY3g8+hkFfSpYj6ZW7pBis6h5hCQ+yb5lxKqDphmWJZT43PY+pMkpPSEzEugSv0VBVAo+O83revAV7+jVHdnTV8yH+N1sQAS1/0B25V7yHTynELjksuH00o+9nD4m9dUEECBhWV4vHNsDhjDghivgcdNeJfCbM2OZrFJUt4aSu9Sx8WrumscomafhJ+rNQKsm5nCmaiUb5SclpVszvAharpfi4bVkbiYFrb0hBWl6sTfqvJsGaSHtFbF88MFeQuB7FJma4HCw7PVWM8/VSku53T9sKAUxA40H4rGEG4qAXJHyRCvR20Mw5OFMWkVir8TPPvdi2ITiGVvSDdv/yX+EGw0NL7yrMFTvciQuxJQObs5gTiwwGUCL1ylwNGNN/4h5GEn0l56M7iYvp9XFYcC1BwFxF8Agt9m36vK9G7ccmI96A02Nz1/uNmiTZ+2OjMEyYfqX9zqr152/OhxFjEJyPjYQvivaLyVlHmbtQ//cW/3BtRQgnl5NZxt6Sr9dbZUvYdNoAqjzzxe5XHhB9SbeQuqr3109FbALBMjHCLXkAbLCIvVKTGO7GP2PuSE8CUe0xzdS4RTS83eEUmgkWW7vKVXO7y+BcPjBK+YEzTdRYkOQzBIoelvtZzGZ9WCPv72p97UF+JadyEEyu5cxSfe2EBLx+hUdH3H/wNpTKmGSFeQSayqrpz8kUt2d43jHQHmjam8aug8jYJ59MYSNV4ZTpYZg9Xn53+zqDnb0/X/KjcAUR0PZbmwVHUjomkaAnWBTdqQcYRgW3mXJegqJbQCJKtzAFgx6UJGncIHcdVJUSPBZOkYohsk8fXfOD37VnJFlRIKeWNTfzjbFV3BD1J7Q91Lqw7VX5T0nB0wM0PtotGhNjOETMhGQMPFzWtJ1zcdPTgytpp64ShRDIKt+ekXb3YQBMSTxQURnF2jxQLCjOeC3tWzJwa+uV0tu3W0Rq2Utn6N3Hlfk/9RU2PE+rFwif86gBXQKfENBiVTqDzO9qiwc2ACN/nTljDzBPMrJfAx+bSIWTLfbxGXiQ4zELlb7otiGCPn/0+s7bMHFWbdPviLGd1RCXteuyEJ3ICsRdCk0CzGObCzwOv93zou5s5qlqUA9WXhfSqUluWNdkwRPrP1A3IQHcNSnoSEbH34fnuwZSWz+bsidpaDeuFT7nQzAXfvRj2P67SYD1CWid6P0iK6IhXiN4AfhpIfYWDKg4fwVQXGuSkVmCrd5i5y6oznE/OlDelLfX6u5NDDh7oKs+ZI04YKD0GoI4hepa9TjO2R8qxs3lN2VWl/nJlGcVzo7w28Mn3Qe0/XeaaX20USkGoxyTTzHOd67wS3Mz1Gh/HilAstuWwN65Q95IkY3F1it/VfrPGmgbsDST50u7YFshLJmoSsALec8+hwcBFiCz2eXJDSyO9dK4R8FxSsat0iDnrI3eAnDzwsx286j5kM9q8lWJ12FnN2BxhQmgihbjXIa4IN/15EpEZlA+aMwlt5EEMm+Iqp/gqLd06p4ThMd+jUT/WuBecADxBA2qDE8ShuT3CESvXdIQr+kkJanZLQbvepNKQ5R9DB1JuYtGBF6uiVdGslXmcw9CSZ3eUUmusx12qKW5nXLPwGivenc86HkX+z5IKGSC3hf+fjNmaf/ul51dtQIikAVkM3KVTZ7BPo0HReQlGN1ETiK/dkYYSe5xHfpTn5fJ8gMhdyfXvr3mUaHc3KTKfMGAgnvTNneh924a/4yMMgZX4w9HysgjTZn14HTfcsRdaPVpCOda59aX/KHTLOgGtaT1PTnflPHEaReSbYzJFvMLb2swnoYv2tV1owYmaP+7b7KNYOWee0O74xnHLWYurUJm85W6QsI+qTKO+YZAEfvh4AL+/0/9lVFp0Y49F4LWqxTC2gV4yOH9X74cPgwTVkDmSYTK0sEksBN5ZwZ+iSiZ9wBHyL6YSzPd8OarZyiPAPEYq2par3DViuAfcBJ5zw+XJUvXtQnUBExVh3cet1V+FgMfduoyVGwMhyeifG8wLuy5o2JHj4g5uVvrW59UpB932OnBptL0IKD0Hi1D8q6VJL2Ulk6LDH3Vip33nlGKSoQUOtPSdRUlMIlAddlrAJl3aOf0/SXx+jo0f86brlos374eG+sgDn0eiIxtTY+rIJbdM6rV96/MJttCZ/ps1lg1uM7Zzho7oZ0cMhm6AMnJOD/5COQKRS4zZj84XD64gDq3mX9T34vVI1NXx+8w3I2sGMBoMzNgZ8uBELpz/gH8+8zK34cyxQSx94qx/taKPR/IrkDbvHNrq+8CDJgrxYDtZ87D9MR8PqrmmhRBtIyHlFsbTYesXnET4zjX2MLd8r0jCc2OLks8RFjO6N94CeZcjBJaXvElTB7WSWbxiUZ0DcUodYh8xoaYxaNTJA5oyAwnM0YUeuvu60LHXwTz6a1CYHDchFsrvMHlK449JBTXpitBuLSMBvURcAI1BEmd6by1FbJbnf3poTKyUPX18D3SwZbwYgp7oER0gz3KG5IpPPZXor+Tk3qbKAeUJOKIxsyRnQ3jvmRfgSrRiOIvSDT+HiowVNDqDH9cwTDcHKm3aPtKFbPHAmqHpGZmGtysPJ7gKueFrFAMSjHrTH40FeUDIYeLHyzDUIc5aXt2rCKXepZyp7PL4O61Ts2KoCEFUwnEMAWXzEcNLRFdYBKSuECpPJJq7YluEfGI9snaDjp3Ksx3tfSIc8sF3+z9SBh1Vko7RR24IHyvHW5SXM3+ZYaT8VRaGmEhqUAvfx9OMG/6AUK2cgahbA/B/L1koHXRYk1rLZ0vCFBtTk+PBpQk4zmLfwUcOHthYbl5CWwcAC0ko4122hmKJ2z1AlNcVg5hzclBXAsMYSVbk8D697tUSf/y6TrRVt5q01s7GoSieVaWWwTyrUTGm3PmL1uItdLGLBEUKpEq3AHCUNZXYxmyNfqF7788jdM1QyV5kF5ypzGuSZnT5L0885IBoiLFlZxUK9C7tPZWGceFEgt1dhiZpTypsemQo13pruYoANQ4lmKARd7T7Je0HU4XpVQRa2vs3uMO7f/bAELYNzfqUD5k9AscCqMSeHNM8GwQLGbG5f3OS2lgLw3LpQxqKWSCELXAPS7D5EtO3+Ef7nWLQ07HQG0SP7scbUUeT9+l5VO0Ry9rp6Vw+WvD19dhAq8gBiliFffmXopoPqQ/1rC4Fw3t0elWJQFybVD17LEisZfxz6hxSW+bwashf/hzHOz93WCzckasxVYNx5zOfDh07TUs8Hqy89idNKXDsZ67NyFKJajLeXqsnhJfltNUkXCN6K6/fGCWYoL0i/p76c4fHWARom+Vsixz95g9buey0kjquyuKSh2/hlfECXx0Yb0cukpoddAme3dL52cZjjHGyXfR5vRmHIsb+G+IyC5BOssVv+0CIy1VUMeylk10J/C8gb6JyhgSwzcos29GpaiKYUEwz/RrH4n6JxvDo+2oLFKKQcrOPmfLNknEI9/4th8Ag1RQZKBJrgtIGnJS9t4UBe8LCXD76TAg1DsEOFrTiHLZBH4zgZQHcTiB98ZS5HZiaZOtuNDEW5xvsUS0kdophaG4DdueebEt4kMaE/mQS81RyzppvLK8iLVaug2KbPPmLVjl2yWEdzfMIhNuPX49D4A/xCi3sKSVDLGY2r5N3vuIBqimRuBEo4Cq4d8bxl+zcM+USbtkEyEMA9yDx256H6cWdKUMb6w2hVH+UM34ceL6C8tWp+aNOVyq18powusAJ3KM4mwqpz2vsjNaRaXEwb8njI3OvjVT3AwC5/KzuoAAq2LAA3rQuB/JA2G/pw9tvZKmgjG8gsCvSUfAu8e0cuOEtbU7/m6kFW4z+W+5OsA2muPVybc1buSLXW0WGlUeAfjVoPTynLH+0oKfjyq3LU8O1M1z99Glxt2+2gYlgtG3N+rmxXkmLdzW5snRvhzqKnIicHUlJYJn7ArHUjDmceWv/197bNO2S7/xOi9rIQEhdplGRCb25jnO0A6CbJ8khNf0hWGcubWNhh7KnYLJs4nb+iLjb0dCA2t6/dOwcM1dD9stj1jDB4Tayk9FDbs3ZFVFI2K9Fd1QUZ1Qk2iXCCpnrwunZH86T9cpLSTFhvoqaUfVStbzd6oRAHh5ULWcyNLXJFLkkLTiG+vsOc+cUtXL98qFQgYi7x0dE0bpc46pn5pHd8W4BG+c7uf3FBUwLJ+2t2uZom/TIGpbfTvSluTX7btOe5W6SP+DBxP3UUqmGwAVUGW1VSFaZWZ9N0ykhhz9+7dfXZa73QQ6dzNqPEd09UwdmUq35JiglNT6v5wGNj12/waBZzJ/3mDdTk/957ncpDz4T8EktMJtfkgojvcrmY8vqV9Tn8jTffmKDqxwouzf0yG0Mvpho+zhKIblRZOCOH81FbkTcFzQx7dNBKL7yqJjiDXgazetV4JRfwE7kf7qO9UHVNuRhzOnNfDPeCVb3TCStR7MZY1LknSiy5xIK4YCCYUNgarj/Q7h6T8nZkWKIDUIIwhYSdxuxXAIT18HDVZ/PhggbSg5/wIcl/gGFhx81Q3evbGtKlZy+R+ldAZVsm0opRZGBIrHrDuIAMY8UlWOvYca5n/5ZbEZEGbHYX0271QXhu+SO4Mpo5ALxxpbCbbwr7aYABJMlfVq4y0oBbq5Nk1/0JiBVhcNm7n5HQ5kjXFfh167iqsQ6KznYyDA3tbbT+xnM7WYf2ai2209NVzhPMEzCScsxwwwlFHBuF6LmPxdE+4d4TyOgia7/Rd1PlihhZyphl9fXDDYHmWSJ3ukWxFPUGNNqEonK8AkgQJPK9+4DOkU1PkXvlHILurWCzZ2/mBms2P9jkZE4bv+kulb+khgLMsimbjzEpEVgVHDD/8wYcufvBoP5QmK2WDKIYv+LbBFCvfCn2XOJ+eMDQkEKKd5390dHOT0Fdf6azWMlp37ZWbDbXDoqsKlAEMQUOKxHuyF7IXoAL7lpa5i1XW0e75LQ09TcJaqDVKHxf1XhLtGaWHxLZ6+VGIVsmhSubNAwRNnOZ9wdiSPNcPQ+RLRCEZ2NGDV9jZcUAq0dQ+QSykrw9C+QHeqMhVw6rMCRjxCV9clwtjIPRx/CT+jSQEZbzRvNLh3t99LlucK6z1JlyBsg4tQPHflMu1dMVi08mYsFX/XW1uZ8e28sqfaC6ScWmUqSKxgB0m+qRJo77EMYA+XK7Pdy/MjsOB3xUjYSOUK1rtUWrLyPKzNAylUdw9YUOs5u7D2q/UEfgW5V2sLQPrWq/gm7u/NY9W6CyUEdDDdPRnFCT9ax/kpnfeesqIZpZHgDPFFYc8rRoEPIESK/iA25ZrmgUs0b06/FfrbKm2SRa8GYL7bvSAjdMH6slib6eJhwPql5aNgUjC/VrgYCgHvm8E1uJgScmEzGgmzAxm6AkjyXcRATF6WELqRTZPRjmbhJaW1q4G1/BWu9wbL0uGepDVcr8CY+bs3Z2R2Ke6JkcXvNZvXG+yJwBNFwJ6Y0StO2K+bRRMzHkYm7qV1BjaXm6b50rszWqaLgfBraLVrVyx0R95uTEfCF+tJNeg/yzQy31kDtPINYB4Hbw4uUVTFlwtwuJ3r7VDMzg2wgCQYFig/fu/6IICgiqVIl1KEDbE0SWbor+cuXMmLRbgY6rUlGlDoXMrLhsTdhzOt8etZ0f8P7UbpYjD1m35RWDlDwwkWB14qDA4XIgeEj041lmPAUVR1ZJkD5S+dA4hasRs5R1W2kzED/cKzbUaT0yQzBtD7v/MJJ2/3uK9WjSA1CmqoHdUNPAby8MMbwbgxqUi8D6Wmwjao65Jz3wtF/NQEaS4pqXR2AxkFCBGUW5XO0X4nt5f7EjTkZdpn1Km5f7ThnQLW2NA5GybfnAo+spxeZ3tez0UjSex4qtnQImGzeGdEiEOPjM53DgMjU3cRrzJqPdOnN45FLuhRNvJQldbz5mZpvj7IRkRiJTzva3/7cT/y0QG06PGArJly89LaV62PWjcY3jGf7VwZxgkYoF6xHabJ6o1tSoQ9N6JfjsqOWtw0KTgbUv5tOr5l6ddWrSPPLYK2+3FyqOEUmxHoe4uobYqFdkpXbl1WxdOtoXWgLU/5p9NvOeZn1IfK5KUM75T2R0dO4KfefeFcQtTr0UsEk54/Z9GZL5GdSkMrhNP5Q7OGeTjqw8UgXJE4mbITwtDhmFNB6fNjGUKaXvGBRq1xjk/CwnWRiwW8w5ytTMtBn4vd3pItHI33jCgXgBU8G8j1n//Sq6L0GDqM7SZvVyIhEUP8jEfftJ8sVzwP7rKKW806jQwlTaPIQRvmBll5U+2lCqjM1wBd62nMmcW7J9cxwZhAazH5n4Y+us7fAvZCXrCi1/KX/O0hrzF1R6wWty7uM/v4YkmGY8VajcqwCAwufkK93JhCh8dheO7BttqFNuLD81N7wFjL4N7SSfAv9NjkjLcAE+daWCX9SiTT/C4ul0AEI90pm+57l0Z5OrN93ZJknIr4zyEhqi5e5cW2qm07OVNQ7oxyvD8kEtWqHhERY0hTTraV4rVG4I2pwHFbfaSHw08j2VLj2xzPDgA1X2iZudm1shuJIzcibo3NZdySX5zLsYGRb5vN/a4hRrekXxZ01i7xLdE3ZjpJwJGQDc5FNmkTxAQUjOFy+WhwnozcuG2/PjdsF8BaGulGAa4OHK+C0vfEEiq6CQvAuuLolTqsdWU1W+cQSmfNUg4V8mbReSheaTqCzj2yirpS/wfukzABXUS8xRCXvDv7D1nV0w4MXi/K8rT3Qjlsyk7AlLYFEZ4qC9gQXLc7hUBwq06EV5QlNya15ZPpNJs2SlZ0zO7MrnD/JuRPuPwb8cYOMtpHEPR/X5mp8uGzR2zMpckIAC995pozxL6lQABqDPNVAAAvzUJP7HEZ/sCAAAAAARZWg=="

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        # We don't want a KeyboardInterrupt throwing a
        # traceback into stdout.
        pass
