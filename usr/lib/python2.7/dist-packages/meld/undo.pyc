ó
hVc           @   sI   d  Z  d d l m Z d e f d     YZ d e j f d     YZ d S(   sD  Module to help implement undo functionality.

Usage:

t = TextWidget()
s = UndoSequence()
def on_textwidget_text_inserted():
    s.begin_group()
    if not t.is_modified():
        s.add_action( TextWidgetModifiedAction() )
    s.add_action( InsertionAction() )
    s.end_group()

def on_undo_button_pressed():
    s.undo()
iÿÿÿÿ(   t   GObjectt   GroupActionc           B   s)   e  Z d  Z d   Z d   Z d   Z RS(   sE   A group action combines several actions into one logical action.
    c         C   s    | |  _  | j d j |  _ d  S(   Ni    (   t   seqt   actionst   buffer(   t   selfR   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyt   __init__(   s    	c         C   s'   x  |  j  j   r" |  j  j   q Wd  S(   N(   R   t   can_undot   undo(   R   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR   .   s    c         C   s'   x  |  j  j   r" |  j  j   q Wd  S(   N(   R   t   can_redot   redo(   R   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR
   2   s    (   t   __name__t
   __module__t   __doc__R   R   R
   (    (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR   %   s   		t   UndoSequencec           B   sã   e  Z d  Z i e j j d e j f f d 6e j j d e j f f d 6e j j d e j e j f f d 6Z	 d   Z
 d   Z d   Z d   Z d   Z d	   Z d
   Z d   Z d   Z d   Z d   Z d   Z d   Z RS(   s?   A manager class for operations which can be undone/redone.
    s   can-undos   can-redot   checkpointedc         C   sA   t  j  j |   g  |  _ d |  _ i  |  _ d |  _ t |  _ d S(   s&   Create an empty UndoSequence.
        i    N(	   R    R   R   t	   next_redot   checkpointst   Nonet   groupt   Falset   busy(   R   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR   A   s    				c         C   sr   |  j  d k s t  |  j   r4 |  j d d  n  |  j   rS |  j d d  n  g  |  _ d |  _ i  |  _ d S(   sö   Remove all undo and redo actions from this sequence

        If the sequence was previously able to undo and/or redo, the
        'can-undo' and 'can-redo' signals are emitted.

        Raises an AssertionError if a group is in progress.
        s   can-undoi    s   can-redoN(	   R   R   t   AssertionErrorR   t   emitR	   R   R   R   (   R   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyt   clearK   s    		c         C   s   |  j  d k S(   s'   Return if an undo is possible.
        i    (   R   (   R   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR   \   s    c         C   s   |  j  t |  j  k  S(   s&   Return if a redo is possible.
        (   R   t   lenR   (   R   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR	   a   s    c         C   s:  |  j  r d S|  j d k r&|  j | j  r^ |  j |  j | j d <|  j d | j t  nL |  j j	 | j d  \ } } | d k	 rª | |  j k rª d |  j | j <n  |  j
   } |  j   } g  |  j |  j )|  j j |  |  j d 7_ | s
|  j d d  n  | r6|  j d d  q6n |  j j |  d S(	   sÓ   Add an action to the undo list.

        Arguments:

        action -- A class with two callable attributes: 'undo' and 'redo'
                  which are called by this sequence during an undo or redo.
        Ni   R   s   can-undos   can-redoi    (   NN(   NN(   R   R   R   R   R   R   R   R   R   t   getR   R	   R   t   appendt
   add_action(   R   t   actiont   startt   endt
   could_undot
   could_redo(    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR   f   s&    	c         C   só   |  j  d k s t  t |  _ |  j |  j  d j } |  j |  rZ |  j d | t  n  |  j	   } |  j  d 8_  |  j |  j  j
   t |  _ |  j   s± |  j d d  n  | sÊ |  j d d  n  |  j |  rï |  j d | t  n  d S(   s[   Undo an action.

        Raises an AssertionError if the sequence is not undoable.
        i    i   R   s   can-undos   can-redoN(   R   R   t   TrueR   R   R   R   R   R   R	   R   R   (   R   t   bufR!   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR      s    		c         C   sþ   |  j  t |  j  k  s t  t |  _ |  j |  j  j } |  j |  r_ |  j d | t	  n  |  j
   } |  j |  j  } |  j  d 7_  | j   t	 |  _ | s¶ |  j d d  n  |  j   sÕ |  j d d  n  |  j |  rú |  j d | t  n  d S(   s\   Redo an action.

        Raises and AssertionError if the sequence is not undoable.
        R   i   s   can-undos   can-redoi    N(   R   R   R   R   R"   R   R   R   R   R   R   R
   R	   (   R   R#   R    t   a(    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR
      s     	
	c         C   sÕ   |  j  } x4 | d k r? |  j | d j | k r? | d 8} q W|  j  } xA | t |  j  d k  r |  j | d j | k r | d 7} qL W| t |  j  k r« d  } n  | | g |  j | <|  j d | t  d  S(   Ni    i   R   (   R   R   R   R   R   R   R   R"   (   R   R#   R   R   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyt
   checkpoint²   s    	)		c         C   sd   |  j  j | d  \ } } | d  k r+ t S| d  k rI t |  j  } n  | |  j k oa | k SS(   N(   NN(   R   R   R   R   R   R   R   (   R   R#   R   R   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR   ¿   s    c         C   s6   |  j  r d S|  j r& |  j j   n t   |  _ d S(   s{  Group several actions into a single logical action.

        When you wrap several calls to add_action() inside begin_group()
        and end_group(), all the intervening actions are considered
        one logical action. For instance a 'replace' action may be
        implemented as a pair of 'delete' and 'create' actions, but
        undoing should undo both of them.
        N(   R   R   t   begin_groupR   (   R   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR&   É   s
    			c         C   s±   |  j  r d S|  j d k	 s" t  |  j j d k	 rD |  j j   ni |  j } d |  _ t | j  d k r |  j | j d  n+ t | j  d k r­ |  j t |   n  d S(   s   End a logical group action. See also begin_group().

        Raises an AssertionError if there was not a matching call to
        begin_group().
        Ni   i    (	   R   R   R   R   t	   end_groupR   R   R   R   (   R   R   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR'   Ú   s    			c         C   sQ   |  j  r d S|  j d k	 s" t  |  j j d k	 rD |  j j   n	 d |  _ d S(   s   Revert the sequence to the state before begin_group() was called.

        Raises an AssertionError if there was no a matching call to begin_group().
        N(   R   R   R   R   t   abort_group(   R   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR(   ï   s    	c         C   s   |  j  d  k	 S(   N(   R   R   (   R   (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyt   in_grouped_actioný   s    N(   R   R   R   R    t   SignalFlagst	   RUN_FIRSTR   t   TYPE_BOOLEANt   TYPE_OBJECTt   __gsignals__R   R   R   R	   R   R   R
   R%   R   R&   R'   R(   R)   (    (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyR   7   s$   %	
				!				
			N(   R   t   gi.repositoryR    t   objectR   R   (    (    (    s-   /usr/lib/python2.7/dist-packages/meld/undo.pyt   <module>    s   